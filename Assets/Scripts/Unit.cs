using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    private Vector3 m_direction;

    private void Update()
    {
        transform.position += m_direction * 3 * Time.deltaTime;
    }

    public void SetDirection(Vector3 _dir)
    {
        m_direction = _dir;
    }
}
