using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Castle : MonoBehaviour
{
    private Vector3 m_spawnPoint;
    [SerializeField] private GameObject m_gameObject;
    [SerializeField] private GameObject m_targetObject;
    private float m_time;

    // Update is called once per frame
    void Update()
    {
        m_time += Time.deltaTime;
        if (m_time > 1.0f)
        {
            m_time -= 1.0f;
            m_spawnPoint = m_targetObject.transform.position - transform.position;
            GameObject _obj = Instantiate(m_gameObject, transform.position + m_spawnPoint.normalized, Quaternion.identity);

            Unit _unit = _obj.GetComponent<Unit>();
            _unit.SetDirection(m_spawnPoint.normalized);
        }
    }
}
